import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import { IMAGES_LINK, API_LINK } from '../../utils/constants'

//const metaDecorator = require("../../data/metaDecorator");
const MetaDecorator = (title, description, imageUrl, imageAlt ) => {
    console.log("test meta", title, description, imageUrl, imageAlt)
    // console.log('test1',title)
    // console.log('test2',description)
    // console.log('test3',imageUrl)
    // console.log('test4',imageAlt)
    // if(title  != undefined& description != undefined & imageUrl != undefined & imageAlt != undefined) {
    //     console.log('trueee')
    // }else {
    //     console.log('falseee')

    // }
    if (title != undefined & description != undefined & imageUrl != undefined & imageAlt != undefined) {
        return (
            <Helmet>
            <title>{title}</title>
            <description>{title}</description>
            <meta property="og:title" content={title} />
            <meta name="title" content={title} />
            <meta name="description" content={description} />
            <meta name="site_name" content={'Maracana Foot'} />
            <meta property="og:description" content={description} />
            <meta property="og:image" content={API_LINK + imageUrl} />
            <meta
                property="og:url"
                content={"https://maracanafoot.com" + window.location.pathname + window.location.search}
            />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:image:alt" content={imageAlt} />
            {/* <meta name="twitter:site" content={metaDecoratortwitterUsername} /> */}
        </Helmet>

        );
    } else {
        return (
            <Helmet>
            </Helmet>
        );
    }
}
MetaDecorator.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    imageAlt: PropTypes.string.isRequired,
};
export default MetaDecorator;



