import React, { Component } from 'react';
import { Container, Row, Col, Input, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import moment from 'moment'
import 'moment/locale/ar'  // Pour la langue ar
import { Markup } from 'interweave';
import { API_LINK } from '../../utils/constants';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index'
import { EmailShareButton, FacebookShareButton, TwitterShareButton, FacebookIcon, TwitterIcon, EmailIcon, WhatsappIcon, WhatsappShareButton, FacebookShareCount } from "react-share";
import DebatCard from '../../components/DebatCard/card.js';
import { ImpulseSpinner } from "react-spinners-kit";
import { AiOutlineLink } from "react-icons/ai";
import { FacebookProvider, Comments } from 'react-facebook';
import { setCookie } from '../../utils/functions';
import { faEye } from '@fortawesome/free-solid-svg-icons'
import MetaDecorator from '../../components/metaDecorator/metaDecorator'
import MetaTags from 'react-meta-tags';
import './index.css';

moment.suppressDeprecationWarnings = true;
moment.locale('ar')
const imageUrl = require('../../components/logo.png')

class ArticlePage extends Component {
  constructor(props) {
    super(props);
    //document.title = 'Maracana - Article';
    this.IDArticle = ''
    this.Datepublié = ''
    this.DateMAJ = ''
    //----- center Spinner -------
    this.styleSpinner = {
      display: "flex",
      justifyContent: "center",
      alignItems: "center"
    }
    //----------------------------
    this.state = {
      article: [],
      comments: [],
      description: '',
      descriptionR: '',
      descriptionUpdate: '',
       shareUrl: window.location.href,
      loadingArticle: true,
    }
  }



  componentWillMount = async () => {
    this.IDArticle = this.props.match.params['id']
    // fix Lang
    this.Lang = this.props.lang.language === 'fr' ? 'FR' : 'AR'
    if (this.Lang == undefined) this.Lang = 'FR'
    if (this.props.location.state) {
      if (this.props.location.state.isSearch === true) {
        this.countSearch()
      }
    }

    this.countVisit()
  }
  myFunction() {
    var copyText = window.location.href;
    navigator.clipboard.writeText(copyText);
  }
  componentDidMount = () => {
    // const script = document.createElement("script");

    // script.src = "https://connect.facebook.net/fr_FR/sdk.js";
    // script.async = true;
    // this.div.appendChild(script);
    // document.body.appendChild(script);
    //   const FB = window.FB;

    //   if(window.FB){

    //     window.fbAsyncInit = function() {
    //       window.FB.init({
    //         appId      : '3435840523158584',
    //         xfbml      : true,
    //         version    : 'v8.0'
    //       });
    //       window.FB.AppEvents.logPageView();
    //     };

    //     (function(d, s, id){
    //        var js, fjs = d.getElementsByTagName(s)[0];
    //        if (d.getElementById(id)) {return;}
    //        js = d.createElement(s); js.id = id;
    //        js.src = "https://connect.facebook.net/en_US/sdk.js";
    //        fjs.parentNode.insertBefore(js, fjs);
    //      }(document, 'script', 'facebook-jssdk'));

    // }

    const fbSDK = (d, s, id) => {
      let js;
      const fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = '//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.6&appId=357191748915887';
      fjs.parentNode.insertBefore(js, fjs);
    };
    fbSDK(document, 'script', 'facebook-jssdk');

    window.FB.XFBML.parse();
  }
  componentDidUpdate() {
    window.FB.XFBML.parse();
  }
  countVisit = async () => {
    let URL = API_LINK + 'v1/articles/countVisit/' + this.IDArticle
    await axios.get(URL)
      .then(this.getLangArticle())
      .catch(err => console.log(err)
      )
  }
  countSearch = async () => {
    let URL = API_LINK + 'v1/articles-countSearch/' + this.IDArticle
    await axios.get(URL)
      .then(this.getArticle())
      .catch(err => console.log(err)
      )
  }
  // ------------- get ---------------- 
  getLangArticle = async () => {
    const URLGet = 'v1/articleLang/';
    await axios.get(API_LINK + URLGet + this.IDArticle)
      .then(response => response.data)
      .then(data => {
        this.Lang2 = data
        if (this.Lang2 !== this.Lang) {
          this.changeLanguage(this.Lang2.toLowerCase())
        }
        this.getArticle()
      })
      .catch(err => {
        console.log(err.response)
        this.setState({
          loadingArticle: false
        })
      })
  }
  getArticle = async () => {
    const URLGet = 'v1/articles/';
    await axios.get(API_LINK + URLGet + this.IDArticle + '/' + this.Lang)
      .then(response => response.data)
      .then(data => {
        this.setState({
          article: data,
          comments: data.comments,
          loadingArticle: false
        })
        // this.getCommentOfArticle(this.IDArticle)
      })
      .catch(err => {
        console.log(err)
        this.setState({
          loadingArticle: false
        })
      })
  }
  changeLanguage(to) {
    setCookie('lang', to, 10);
    window.location.reload();
  }
  getCommentOfArticle = (IDArticle) => { //pas utiliser
    const URLGet = 'v1/articles-comments/';

    axios.get(API_LINK + URLGet + IDArticle)
      .then(response => {
      })
      .catch(err => console.log(err))
  }
  //------------- FIN get ------------------
  // ---------- Model 'form' d'article ----------
  calculdatePublié = () => {
    if (this.state.article.date) {
      let datePublié = this.state.article.date

      if (moment(datePublié).fromNow() === 'il y a un jour') {
        if (this.Lang === 'FR') {
          this.Datepublié = 'hier à ' + moment(datePublié).format('HH') + 'H' + moment(datePublié).format('MM')
        } else {
          this.Datepublié = 'أمس على الساعة ' + moment(datePublié).format('HH') + ' و ' + moment(datePublié).format('MM') + 'دقيقة'
        }
      } else if (moment(datePublié).isBetween(moment().subtract(24, 'hours'), moment().subtract(-24, 'hours'))) {
        if (this.Lang === 'FR') {
          this.Datepublié = moment(datePublié).fromNow()
        } else {
          this.Datepublié = moment(datePublié).lang('ar-dz').fromNow()
        }
      } else {
        if (this.Lang === 'FR') {
          this.Datepublié = 'le ' + moment(datePublié).format('DD/MM/YYYY') + ' à ' + moment(datePublié).format('HH') + 'H' + moment(datePublié).format('MM')
        } else {
          this.Datepublié = moment(datePublié).format('YYYY/MM/DD') + ' على الساعة ' + moment(datePublié).format('HH') + ' و ' + ' ' + moment(datePublié).format('MM') + 'دقيقة'
        }
      }

      //-------------------
      let dateMAJ = this.state.article.dateUpdate
      if (moment(dateMAJ).fromNow() === 'il y a un jour') {
        if (this.Lang === 'FR') {
          this.DateMAJ = 'hier à ' + moment(dateMAJ).format('HH') + 'H' + moment(dateMAJ).format('MM')
        } else {
          this.DateMAJ = 'أمس على الساعة ' + moment(dateMAJ).format('HH') + 'سا و ' + moment(dateMAJ).format('MM') + 'دقيقة'
        }
      } else if (moment(dateMAJ).isBetween(moment().subtract(24, 'hours'), moment().subtract(-24, 'hours'))) {
        if (this.Lang === 'FR') {
          this.DateMAJ = moment(dateMAJ).fromNow()
        } else {
          this.DateMAJ = moment(dateMAJ).lang('ar-dz').fromNow()
        }
      } else {
        if (this.Lang === 'FR') {
          this.DateMAJ = moment(dateMAJ).format('DD/MM/YYYY') + ' à ' + moment(dateMAJ).format('HH') + 'H' + moment(dateMAJ).format('MM')
        } else {
          this.DateMAJ = moment(dateMAJ).format('YYYY/MM/DD') + ' على الساعة ' + moment(dateMAJ).format('HH') + 'سا و ' + moment(dateMAJ).format('MM') + 'دقيقة'
        }
      }
    }

  }
  spanDate = () => {
    if (this.state.article.date && this.state.article.dateUpdate) {
      return (
        <>
          <span style={{ color: '#333333' }}>{this.Lang === 'FR' ? 'Publié ' : 'تم النشر '}</span>
          <span className={this.Lang === 'FR' ? "temps-pub font-weight-bold" : "temps-pub2 font-weight-bold"}>
            <b>{this.Datepublié}</b>
            {' | '}
          </span>
          <span style={{ color: '#333333' }}>{this.Lang === 'FR' ? 'Dernière mise à jour ' : 'تم التحديث '}</span>
          <span className={this.Lang === 'FR' ? "temps-pub font-weight-bold" : "temps-pub2 font-weight-bold"}>
            <b>{this.DateMAJ}</b>
            <b className="ml-3 text-dark"><FontAwesomeIcon icon={faEye} /> {this.state.article.countVisit}</b>
          </span>
        </>
      )
    } else {
      return (
        <>
          <span style={{ color: '#333333' }}>{this.Lang === 'FR' ? 'Publié ' : 'تم النشر '}</span>
          <span className={this.Lang === 'FR' ? "temps-pub font-weight-bold" : "temps-pub2 font-weight-bold"}>
            <b>{this.Datepublié}</b>
          </span>
          <b className="ml-3" style={{ color: '#333333' }}><FontAwesomeIcon icon={faEye} /> <span style={{ color: '#000000' }}> {this.state.article.countVisit} </span></b>
        </>
      )
    }
  }
  renderArticle = () => {
    return <>
      <img src={API_LINK + this.state.article.image} alt="Article Image" className="image-article" />
      <div className="noselect">
        <div className="text-article font-weight-bold">
          <Markup content={this.state.article.content} />
        </div>
      </div>
      {this.renderSocialButtons()}
    </>
  }
  commentaire = () => {
    return (
      this.state.article.isActive ? <>
        <div>
          <Row className="margin0 mt-4 w-100 text-center">
            <Col md="8" className="col-center">
              {this.props.isAuthenticated ? <>
                <div class="fb-comments" data-href={API_LINK + this.props.location.pathname} data-width="" data-numposts="5"></div>

                {/* <Input type="textarea" name="" placeholder={this.Lang === 'FR' ? "Commentez ici...." : "التعليق هنا ...."} onChange={this.handlecomment} value={this.state.description} />
                <Button color="success" className="btn-block mt-1" onClick={this.addComment}>{this.props.lang.t('article.BtnCommenter')}</Button> */}
              </> :
                <Button color="success" className="btn-block mt-1" onClick={() => {
                  this.props.history.push('/login')
                  this.props.onSetRedirectPath("/article/" + this.IDArticle)
                }}>{this.props.lang.t('article.connectez')}</Button>
              }
            </Col>
            {/* <Col md="8" className="col-center titre-ligue1-div">
              <p className="titre-ligue1-p float-left"> {this.state.comments.length} {this.props.lang.t('article.NbrCommentaire')} </p>
              <br />
              <div style={{ marginTop: '-20px' }}>
                <hr />
              </div>
            </Col> */}
          </Row>
          {/* <Row>
            <Col md="8" className="col-center">
              {/* boucle */}
          {/* {this.listDesComments()} */}
          {/* </Col>
          </Row>  */}

        </div>
      </> :
        <Row>
          <br />
          <Col md="12" className='text-center text-danger'>
            <br />
            <p> {this.props.lang.t('article.desactiveComnt')} </p>
          </Col>
        </Row>
    )
  }
  listDesComments = () => {
    let comment = this.state.comments.map(comment => {
      if (comment.user) {
        let dateComment = comment.date
        let Year = new Date(dateComment).getFullYear();
        let M = new Date(dateComment).getMonth();
        let Month = this.GetMonth(M)
        let D = new Date(dateComment).getDate();
        let Day = this.GetDay(D)
        let YMD = Year + Month + Day

        let H = new Date(dateComment).getHours();
        let MM = new Date(dateComment).getMinutes();
        let SS = new Date(dateComment).getSeconds();

        let DateComment = moment(YMD + '' + H + ':' + MM + ':' + SS, "YYYYMMDD h:mm:ss").fromNow()

        return (
          <DebatCard
            lang={{ language: this.Lang }}
            key={comment._id}
            profile_picture={comment.user.picture ? API_LINK + comment.user.picture : comment.user.sexe === "Monsieur" ? require('../../uploads/images/avatar-man.png') : require('../../uploads/images/avatar-women.jpg')}
            username={comment.user.name + ' ' + comment.user.lastname}
            time={moment(comment.date).locale(this.Lang).fromNow()}
            content={comment.description}
            id={comment._id}
            description={this.state.descriptionR}
            handleChange={this.handleChangeCommentR}
            handleClick={this.handleKeyDown}
            isAuthor={comment.user._id === this.props.userId}
            updateComment={this.updateComment}
            deleteComment={this.deleteComment}
            updateValue={() => this.setState({ descriptionUpdate: comment.description })}
            value={this.state.descriptionUpdate}
            update={(e) => this.setState({ descriptionUpdate: e.target.value })}
            isReply={false}
            replies={this.renderResponses(comment)}
          />
        );

        return (
          <div className="card gedf-card">
            <div className="card-header noborder bg-transparent pb-0">
              <div className="d-flex justify-content-between align-items-center">
                <div className="d-flex justify-content-between align-items-center">
                  <div className="mr-2">
                    <img className="rounded-circle" width="45" src={require('../../uploads/images/mahrezpng.png')} alt="" />
                  </div>
                  <div className="ml-2">
                    <div className="h5 m-0">{comment.user.name + ' ' + comment.user.lastname}</div>
                    <div className="text-muted h7 mb-2"> <FontAwesomeIcon icon={faClock} /> {DateComment} </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-body py-1">
              <p className="card-text">
                {comment.description}
              </p>
            </div>

          </div>
        )
      }
    })
    return comment
  }
  // ---------- FIN Model 'form' d'article ----------

  //-------------- autre method --------------
  addCountPartage = async () => {
    let URL = API_LINK + 'v1/articles/countPartage/' + this.IDArticle
    await axios.get(URL)
      .then(this.getArticle())
      .catch(err => console.log(err)
      )
  }
  isEmpty = (obj) => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

  handlecomment = (e) => {
    if (e) {
      this.setState({
        description: e.target.value
      })
    }
  }

  handleChangeCommentR = (e) => {
    if (e) {
      this.setState({
        descriptionR: e.target.value
      })
    }
  }
  handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      if (this.state.descriptionR !== '' && e.target.id.split('#')[0] === 'response') {
        this.addResponse(this.IDArticle, e.target.id.split('#')[1])
      }
    }
  }
  addComment = () => {

    let comment = {
      comment: {
        user: this.props.userId,
        description: this.state.description,
        date: moment()
      }
    }
    const URLAddComment = 'v1/articles-comments/';
    axios.post(API_LINK + URLAddComment + this.IDArticle, comment, {
      headers: {
        'content-type': 'application/json'
      }
    })
      .then(res => {
        this.setState({
          description: ''
        })
        this.getArticle()
      })
      .catch(err => console.log(err))
  }
  updateComment = async (commentID) => {

    const comment = {
      description: this.state.descriptionUpdate,
      user: this.props.userId
    }


    await axios.put(API_LINK + 'v1/articles-comments/' + this.IDArticle + '/' + commentID, comment).then(res => {
      this.setState({
        descriptionUpdate: ''
      }, () => {
        this.getArticle()
      })
    }).catch(err => {
      console.log(err)
    })
  }
  deleteComment = async (commentID) => {
    await axios.delete(API_LINK + 'v1/articles-comments/' + this.IDArticle + '/' + commentID).then(res => {
      this.getArticle()
    }).catch(err => {
      console.log(err)
    })
  }
  addResponse = async (articleID, commentID) => {
    const response = {
      description: this.state.descriptionR,
      date: moment().format(),
      user: this.props.userId
    }
    await axios.put(API_LINK + 'v1/addResponseArticle/' + articleID + '/' + commentID, response).then(res => {
      this.setState({
        descriptionR: '',
        descriptionUpdate: ''
      }, () => {
        this.getArticle()
      })
    }).catch(err => {
      console.log(err)
    })
  }
  updateResponse = async (commentID, responseID) => {
    const response = {
      description: this.state.descriptionUpdate,
      user: this.props.userId
    }
    await axios.put(API_LINK + 'v1/setResponseArticle/' + this.IDArticle + '/' + commentID + '/' + responseID, response).then(res => {
      this.setState({
        descriptionUpdate: ''
      }, () => {
        this.getArticle()
      })
    }).catch(err => {
      console.log(err)
    })
  }
  deleteResponse = async (commentID, responseID) => {

    await axios.delete(API_LINK + 'v1/deleteResponseArticle/' + this.IDArticle + '/' + commentID + '/' + responseID).then(res => {
      this.getArticle()
    }).catch(err => {
      console.log(err.response)
    })
  }
  renderResponses = (comment) => {
    return comment.response.map(response => {
      if (response.user) {
        return <DebatCard
          key={response._id}
          id={response._id}
          lang={{ language: this.Lang }}
          value={this.state.descriptionUpdate}
          updateValue={() => this.setState({ descriptionUpdate: response.description })}
          idC={comment._id}
          isAuthor={response.user._id === this.props.userId}
          updateResponse={this.updateResponse}
          deleteResponse={this.deleteResponse}
          update={(e) => this.setState({ descriptionUpdate: e.target.value })}
          isReply={true}
          profile_picture={response.user.picture ? API_LINK + response.user.picture : response.user.sexe === "Monsieur" ? require('../../uploads/images/avatar-man.png') : require('../../uploads/images/avatar-women.jpg')}
          username={response.user.name + ' ' + response.user.lastname}
          time={moment(response.date).locale(this.Lang).fromNow()}
          content={response.description}
        />
      }
    })
  }
  renderSocialButtons = () => {
    const s = this.state
    return <>
      <div className="text-left float-left mr-4">
        <div className="social-buttons font-weight-bold" >
          <span style={{ marginTop: '12px' }}>
            <Markup content={this.Lang === 'FR' ? "PARTAGER CET ARTICLE" : "شارك هذا المقال"} />
          </span>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <div className="social-buttons">
            <FacebookShareButton url={s.shareUrl} onClick={this.addCountPartage.bind(this)}>
              <FacebookIcon size={32} borderRadius="20%" color="black" />
            </FacebookShareButton>
                  &nbsp;&nbsp;
                  <TwitterShareButton url={s.shareUrl} onClick={this.addCountPartage.bind(this)}>
              <TwitterIcon size={32} borderRadius="20%" />
            </TwitterShareButton>
                  &nbsp;&nbsp;
                  {/* <wHatsappShareButton url={s.shareUrl} onClick={this.addCountPartage.bind(this)}>
                      <WhatsappIcon size={32} round />
                    </wHatsappShareButton> */}
            <WhatsappShareButton
              url={s.shareUrl}
              title={this.state.article.title}
              onClick={this.addCountPartage.bind(this)}
              separator=":: "
              className="Demo__some-network__share-button"
            >
              <WhatsappIcon size={32} borderRadius="20%" />
            </WhatsappShareButton>
            &nbsp;&nbsp;
                  {/* <EmailShareButton url={s.shareUrl} onClick={this.addCountPartage.bind(this)}>
                      <EmailIcon size={32} round />
                    </EmailShareButton> */}

            <EmailShareButton
              url={s.shareUrl}
              subject={this.state.article.title}
              //onClick={this.addCountPartage.bind(this)}
              className="Demo__some-network__share-button"
            >
              <img src="https://img.icons8.com/ios-filled/50/000000/apple-mail.png" width="38" height="38"
              />
            </EmailShareButton>

                    &nbsp;
                    <EmailShareButton
              onClick={this.myFunction} style={{ cursor: "pointer" }}
            >
              <img src="https://img.icons8.com/material-sharp/24/000000/copy-link.png" width="38" height="38" background-color="#fffff"
              />
            </EmailShareButton>
            {/*   <FaShareAlt onClick={this.myFunction} style={{ cursor: "pointer" }} size={28}  >
                    <img src="https://img.icons8.com/carbon-copy/100/000000/link.png"/>
                    </FaShareAlt> */}
          </div>
        </div>
      </div>
    </>
  }
  GetMonth = (M) => {
    switch (M) {
      case 0: M = '01'
        break;
      case 1: M = '02'
        break;
      case 2: M = '03'
        break;
      case 3: M = '04'
        break;
      case 4: M = '05'
        break;
      case 5: M = '06'
        break;
      case 6: M = '07'
        break;
      case 7: M = '08'
        break;
      case 8: M = '09'
        break;
      case 9: M = '10'
        break;
      case 10: M = '11'
        break;
      case 11: M = '12'
        break;
      default:
        break;
    }
    return M
  }
  GetDay = (D) => {
    switch (D) {
      case 1: D = '01'
        break;
      case 2: D = '02'
        break;
      case 3: D = '03'
        break;
      case 4: D = '04'
        break;
      case 5: D = '05'
        break;
      case 6: D = '06'
        break;
      case 7: D = '07'
        break;
      case 8: D = '08'
        break;
      case 9: D = '09'
        break;
      default:
        break;
    }
    return D
  }
  //-------------- FIN autre method --------------

  render = () => {
    const s = this.state;
    console.log("imene", this.state.article)
    return (<>

      <div>
        <MetaTags>
          <title>{this.state.article.title}</title>
          <meta name="description" content={this.state.article.resumer} />
          <meta property="og:title" content={this.state.article.title} />
          <meta property="og:image" content={API_LINK + this.state.article.image} />
        </MetaTags>
        {this.state.loadingArticle ? <>

          <br />
          <div style={this.styleSpinner} className='mt-3 mb-3'>
            <ImpulseSpinner size={30} color="#208FDF" loading={this.state.loadingArticle} />
          </div></>
          : null}
        {!this.isEmpty(this.state.article) ? <>
          <MetaTags>
            <title>{this.state.article.title}</title>
            <meta name="description" content={this.state.article.resumer} />
            <meta property="og:title" content={this.state.article.title} />
            <meta property="og:image" content={API_LINK + this.state.article.image} />
          </MetaTags>
            {console.log('metaaaa',this.state.article.title)}
          {/* <MetaDecorator
            description={this.state.article.resumer}
            title={this.state.article.title}
            imageUrl={this.state.article.image}
            imageAlt="image alt"
          /> */}

          <Container>

            <Row>
              <Col md="8" className="col-center article">
                <div className="tete-article">
                  <h2 className="titre-article text-left">
                    <b>{this.state.article.title}</b>
                  </h2>
                  <div className="noselect">
                    <p style={{ fontSize: '15.5px', color: 'black', textAlign: 'justify', justifyContent: 'start', wordSpacing: '5px', marginTop: '10px' }} className="font-weight-bold  ">
                      <Markup content={this.state.article.resumer} />
                    </p>
                  </div>
                  <br />
                  <div style={{ marginTop: '-28px' }}>
                    {/* <div className="auteur float-left"> */}
                    <div className="float-left">
                      <span className="border-bottom" style={{ color: '#333333', fontSize: '15px' }}>
                        {this.Lang === 'FR' ? 'Par ' : 'بواسطة '}
                      </span>
                      <span className="border-bottom font-weight-bold" style={{ color: 'black', fontSize: '15px' }}>
                        {this.state.article.author === 'maracana foot' || 'Maracana Foot' && this.Lang === 'AR' ?
                          'ماراكانافوت' :
                          this.state.article.author}
                      </span>
                      &nbsp; &nbsp; &nbsp;
                    </div>
                    <div className={this.Lang === 'FR' ? "col-md-12 col-sm-12  col-xs-12 temps-pub px-0 py-0" : "col-md-12 col-sm-12  col-xs-12 temps-pub2 px-0 py-0 "}>
                      {this.calculdatePublié()}
                      {this.spanDate()}
                    </div>
                  </div>
                </div>
                {this.renderArticle()}
              </Col>
            </Row>

            {this.commentaire()}
          </Container>
        </>
          : !this.state.loadingArticle ? <div className='text-danger text-center'> <br /> <br /><h4>{this.Lang === 'FR' ? 'Article indisponible' : 'المقال غير متوفر'}  </h4> <br /><br /></div> : null}

      </div>
    </>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null,
    userId: state.auth.userId
  }
}
const mapDispatchToProps = dispatch => {
  return {
    onSetRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ArticlePage);
