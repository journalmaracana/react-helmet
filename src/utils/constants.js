import moment from 'moment'

export const API_LINK = 'https://maracanafoot.com:10010/';
export const IMAGES_LINK = 'https://maracanafoot.com/utils/images/';
// 51.91.221.140
export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    }
}
export const PHONE_CODE = [
    { value: '+1', label: '+1 ' },
    { value: '+7', label: '+7 ' },

    { value: '+20', label: '+20' },
    { value: '+27', label: '+27' },
    { value: '+30', label: '+30' },
    { value: '+31', label: '+31' },
    { value: '+32', label: '+32' },
    { value: '+33', label: '+33' },
    { value: '+34', label: '+34' },
    { value: '+36', label: '+36' },
    { value: '+39', label: '+39' },

    { value: '+40', label: '+40' },
    { value: '+41', label: '+41' },
    { value: '+41', label: '+41' },
    { value: '+43', label: '+41' },
    { value: '+44', label: '+44' },
    { value: '+45', label: '+45' },
    { value: '+46', label: '+46' },
    { value: '+47', label: '+47' },
    { value: '+48', label: '+48' },
    { value: '+49', label: '+49' },
    { value: '+51', label: '+51' },
    { value: '+52', label: '+52' },
    { value: '+53', label: '+53' },
    { value: '+54', label: '+54' },
    { value: '+55', label: '+55' },
    { value: '+56', label: '+56' },
    { value: '+57', label: '+57' },
    { value: '+58', label: '+58' },
    { value: '+60', label: '+60' },
    { value: '+61', label: '+61' },

    { value: '+62', label: '+62' },
    { value: '+63', label: '+63' },
    { value: '+64', label: '+64' },
    { value: '+65', label: '+65' },
    { value: '+66', label: '+66' },
    { value: '+81', label: '+81' },
    { value: '+82', label: '+82' },
    { value: '+84', label: '+84' },
    { value: '+86', label: '+86' },
    { value: '+90', label: '+90' },
    { value: '+91', label: '+91' },
    { value: '+92', label: '+92' },
    { value: '+93', label: '+93' },
    { value: '+94', label: '+94' },
    { value: '+95', label: '+95' },
    { value: '+98', label: '+98' },

    { value: '+212', label: '+212' },//maroc
    { value: '+213', label: '+213' },//alger
    { value: '+216', label: '+216' },//tunis

    { value: '+218', label: '+218' },
    { value: '+220', label: '+220' },
    { value: '+221', label: '+221' },
    { value: '+222', label: '+22 ' },
    { value: '+223', label: '+223' },
    { value: '+224', label: '+224' },
    { value: '+225', label: '+225' },
    { value: '+226', label: '+226' },
    { value: '+227', label: '+227' },
    { value: '+228', label: '+228' },
    { value: '+229', label: '+229' },
    { value: '+230', label: '+230' },
    { value: '+231', label: '+231' },
    { value: '+232', label: '+232' },
    { value: '+233', label: '+233' },
    { value: '+234', label: '+234' },
    { value: '+235', label: '+235' },
    { value: '+236', label: '+236' },
    { value: '+237', label: '+237' },
    { value: '+238', label: '+238' },
    { value: '+239', label: '+239' },
    { value: '+240', label: '+240' },
    { value: '+241', label: '+241' },

    { value: '+242', label: '+242' },
    { value: '+243', label: '+243' },
    { value: '+244', label: '+244' },
    { value: '+245', label: '+245' },
    { value: '+246', label: '+246' },
    { value: '+247', label: '+247' },
    { value: '+248', label: '+248' },
    { value: '+249', label: '+249' },
    { value: '+250', label: '+250' },
    { value: '+251', label: '+251' },
    { value: '+252', label: '+252' },
    { value: '+253', label: '+253' },
    { value: '+254', label: '+254' },
    { value: '+255', label: '+255' },
    { value: '+256', label: '+256' },
    { value: '+257', label: '+257' },
    { value: '+258', label: '+258' },
    { value: '+260', label: '+260' },
    { value: '+261', label: '+261' },
    { value: '+262', label: '+262' },
    { value: '+263', label: '+263' },
    { value: '+264', label: '+264' },
    { value: '+265', label: '+265' },
    { value: '+266', label: '+266' },
    { value: '+267', label: '+267' },
    { value: '+268', label: '+268' },

    { value: '+269', label: '+269' },

    { value: '+290', label: '+290' },
    { value: '+291', label: '+291' },
    { value: '+297', label: '+297' },
    { value: '+298', label: '+298' },
    { value: '+299', label: '+299' },
    { value: '+340', label: '+340' },
    { value: '+345', label: '+345' },
    { value: '+349', label: '+349' },
    { value: '+350', label: '+350' },
    { value: '+351', label: '+351' },
    { value: '+352', label: '+352' },
    { value: '+353', label: '+353' },
    { value: '+354', label: '+354' },
    { value: '+355', label: '+355' },
    { value: '+356', label: '+356' },
    { value: '+357', label: '+357' },
    { value: '+358', label: '+358' },
    { value: '+359', label: '+359' },
    { value: '+370', label: '+370' },
    { value: '+371', label: '+371' },
    { value: '+372', label: '+372' },
    { value: '+373', label: '+373' },
    { value: '+374', label: '+374' },
    { value: '+375', label: '+375' },
    { value: '+376', label: '+376' },
    { value: '+377', label: '+377' },
    { value: '+378', label: '+378' },
    { value: '+380', label: '+380' },
    { value: '+381', label: '+381' },
    { value: '+385', label: '+385' },
    { value: '+386', label: '+386' },
    { value: '+387', label: '+387' },
    { value: '+389', label: '+389' },
    { value: '+390', label: '+390' },
    { value: '+420', label: '+420' },
    { value: '+421', label: '+421' },
    { value: '+423', label: '+423' },
    { value: '+441', label: '+441' },
    { value: '+473', label: '+473' },
    { value: '+500', label: '+500' },
    { value: '+501', label: '+501' },
    { value: '+502', label: '+502' },
    { value: '+503', label: '+503' },
    { value: '+504', label: '+504' },
    { value: '+505', label: '+505' },
    { value: '+506', label: '+506' },
    { value: '+507', label: '+507' },
    { value: '+509', label: '+509' },
    { value: '+590', label: '+590' },
    { value: '+591', label: '+591' },
    { value: '+592', label: '+592' },
    { value: '+593', label: '+593' },
    { value: '+594', label: '+594' },
    { value: '+595', label: '+595' },
    { value: '+596', label: '+596' },
    { value: '+597', label: '+597' },
    { value: '+598', label: '+598' },
    { value: '+599', label: '+599' },

    { value: '+649', label: '+649' },
    { value: '+664', label: '+664' },
    { value: '+670', label: '+670' },
    { value: '+671', label: '+671' },
    { value: '+672', label: '+672' },

    { value: '+673', label: '+673' },
    { value: '+674', label: '+674' },
    { value: '+675', label: '+675' },
    { value: '+676', label: '+676' },
    { value: '+677', label: '+677' },
    { value: '+678', label: '+678' },
    { value: '+679', label: '+679' },
    { value: '+680', label: '+680' },
    { value: '+681', label: '+681' },
    { value: '+682', label: '+682' },
    { value: '+683', label: '+683' },
    { value: '+684', label: '+684' },
    { value: '+685', label: '+685' },
    { value: '+686', label: '+686' },
    { value: '+687', label: '+687' },
    { value: '+688', label: '+688' },
    { value: '+689', label: '+689' },
    { value: '+758', label: '+758' },
    { value: '+767', label: '+767' },
    { value: '+787', label: '+787' },
    { value: '+809', label: '+809' },

    { value: '+850', label: '+850' },
    { value: '+852', label: '+852' },
    { value: '+853', label: '+853' },
    { value: '+855', label: '+855' },
    { value: '+856', label: '+856' },
    { value: '+868', label: '+868' },
    { value: '+869', label: '+869' },
    { value: '+871', label: '+871' },
    { value: '+872', label: '+872' },
    { value: '+873', label: '+873' },
    { value: '+874', label: '+874' },
    { value: '+876', label: '+876' },
    { value: '+880', label: '+880' },
    { value: '+886', label: '+886' },
    { value: '+960', label: '+960' },
    { value: '+961', label: '+961' },
    { value: '+962', label: '+962' },
    { value: '+963', label: '+963' },
    { value: '+964', label: '+964' },
    { value: '+965', label: '+965' },
    { value: '+966', label: '+966' },
    { value: '+967', label: '+967' },
    { value: '+968', label: '+968' },
    { value: '+970', label: '+970' },
    { value: '+971', label: '+971' },
    { value: '+972', label: '+972' },
    { value: '+973', label: '+973' },
    { value: '+974', label: '+974' },
    { value: '+975', label: '+975' },
    { value: '+976', label: '+976' },
    { value: '+977', label: '+977' },
    { value: '+992', label: '+992' },
    { value: '+993', label: '+993' },
    { value: '+994', label: '+994' },
    { value: '+995', label: '+995' },
    { value: '+996', label: '+996' },
    { value: '+1242', label: '+1242' },
    { value: '+1246', label: '+1246' },
    { value: '+1264', label: '+1264' },
    { value: '+1268', label: '+1268' },
    { value: '+1284', label: '+1284' },
    { value: '+1340', label: '+1340' },
    { value: '+1345', label: '+1345' },
    { value: '+1441', label: '+1441' },
    { value: '+1473', label: '+1473' },
    { value: '+1649', label: '+1679' },
    { value: '+1664', label: '+1664' },
    { value: '+1758', label: '+1758' },
    { value: '+1767', label: '+1767' },
    { value: '+1784', label: '+1784' },
    { value: '+1787', label: '+1787' },
    { value: '+1808', label: '+1808' },
    { value: '+1809', label: '+1809' },

    { value: '+1868', label: '+1868' },
    { value: '+1869', label: '+1869' },
    { value: '+1876', label: '+1876' },
    { value: '+6723', label: '+6723' }]

export const PAYS_OPTIONS = [
    { value: "Afghanistan", label: "Afghanistan" },
    { value: "Afrique_du_Sud", label: "Afrique du Sud" },
    { value: "Albanie", label: "Albanie" },
    { value: "Algerie", label: "Algérie" },
    { value: "Allemagne", label: "Allemagne" },
    { value: "Andorre", label: "Andorre" },
    { value: "Angola", label: "Angola" },
    { value: "Antigua-et-Barbuda", label: "Antigua-et-Barbuda" },
    { value: "Arabie_saoudite", label: "Arabie saoudite" },
    { value: "Argentine", label: "Argentine" },
    { value: "Armenie", label: "Arménie" },
    { value: "Australie", label: "Australie" },
    { value: "Autriche", label: "Autriche" },
    { value: "Azerbaidjan", label: "Azerbaïdjan" },
    { value: "Bahamas", label: "Bahamas" },
    { value: "Bahrein", label: "Bahreïn" },
    { value: "Bangladesh", label: "Bangladesh" },
    { value: "Barbade", label: "Barbade" },
    { value: "Belau", label: "Belau" },
    { value: "Belgique", label: "Belgique" },
    { value: "Belize", label: "Belize" },
    { value: "Benin", label: "Bénin" },
    { value: "Bhoutan", label: "Bhoutan" },
    { value: "Bielorussie", label: "Biélorussie" },
    { value: "Birmanie", label: "Birmanie" },
    { value: "Bolivie", label: "Bolivie" },
    { value: "Bosnie-Herzégovine", label: "Bosnie-Herzégovine" },
    { value: "Botswana", label: "Botswana" },
    { value: "Bresil", label: "Brésil" },
    { value: "Brunei", label: "Brunei" },
    { value: "Bulgarie", label: "Bulgarie" },
    { value: "Burkina", label: "Burkina" },
    { value: "Burundi", label: "Burundi" },
    { value: "Cambodge", label: "Cambodge" },
    { value: "Cameroun", label: "Cameroun" },
    { value: "Canada", label: "Canada" },
    { value: "Cap-Vert", label: "Cap-Vert" },
    { value: "Chili", label: "Chili" },
    { value: "Chine", label: "Chine" },
    { value: "Chypre", label: "Chypre" },
    { value: "Colombie", label: "Colombie" },
    { value: "Comores", label: "Comores" },
    { value: "Congo", label: "Congo" },
    { value: "Cook", label: "Cook" },
    { value: "Coree_du_Nord", label: "Corée du Nord" },
    { value: "Coree_du_Sud", label: "Corée du Sud" },
    { value: "Costa_Rica", label: "Costa Rica" },
    { value: "Cote_Ivoire", label: "Côte d'Ivoire" },
    { value: "Croatie", label: "Croatie" },
    { value: "Cuba", label: "Cuba" },
    { value: "Danemark", label: "Danemark" },
    { value: "Djibouti", label: "Djibouti" },
    { value: "Dominique", label: "Dominique" },
    { value: "Egypte", label: "Égypte" },
    { value: "Emirats_arabes_unis", label: "Émirats arabes unis" },
    { value: "Equateur", label: "Équateur" },
    { value: "Erythree", label: "Érythrée" },
    { value: "Espagne", label: "Espagne" },
    { value: "Estonie", label: "Estonie" },
    { value: "Etats-Unis", label: "États-Unis" },
    { value: "Ethiopie", label: "Éthiopie" },
    { value: "Fidji", label: "Fidji" },
    { value: "Finlande", label: "Finlande" },
    { value: "France", label: "France" },
    { value: "Gabon", label: "Gabon" },
    { value: "Gambie", label: "Gambie" },
    { value: "Georgie", label: "Géorgie" },
    { value: "Ghana", label: "Ghana" },
    { value: "Grèce", label: "Grèce" },
    { value: "Grenade", label: "Grenade" },
    { value: "Guatemala", label: "Guatemala" },
    { value: "Guinee", label: "Guinée" },
    { value: "Guinee-Bissao", label: "Guinée-Bissao" },
    { value: "Guinee_equatoriale", label: "Guinée équatoriale" },
    { value: "Guyana", label: "Guyana" },
    { value: "Haiti", label: "Haïti" },
    { value: "Honduras", label: "Honduras" },
    { value: "Hongrie", label: "Hongrie" },
    { value: "Inde", label: "Inde" },
    { value: "Indonesie", label: "Indonésie" },
    { value: "Iran", label: "Iran" },
    { value: "Iraq", label: "Iraq" },
    { value: "Irlande", label: "Irlande" },
    { value: "Islande", label: "Islande" },
    { value: "Israël", label: "Israël" },
    { value: "Italie", label: "Italie" },
    { value: "Jamaique", label: "Jamaïque" },
    { value: "Japon", label: "Japon" },
    { value: "Jordanie", label: "Jordanie" },
    { value: "Kazakhstan", label: "Kazakhstan" },
    { value: "Kenya", label: "Kenya" },
    { value: "Kirghizistan", label: "Kirghizistan" },
    { value: "Kiribati", label: "Kiribati" },
    { value: "Koweit", label: "Koweït" },
    { value: "Laos", label: "Laos" },
    { value: "Lesotho", label: "Lesotho" },
    { value: "Lettonie", label: "Lettonie" },
    { value: "Liban", label: "Liban" },
    { value: "Liberia", label: "Liberia" },
    { value: "Libye", label: "Libye" },
    { value: "Liechtenstein", label: "Liechtenstein" },
    { value: "Lituanie", label: "Lituanie" },
    { value: "Luxembourg", label: "Luxembourg" },
    { value: "Macedoine", label: "Macédoine" },
    { value: "Madagascar", label: "Madagascar" },
    { value: "Malaisie", label: "Malaisie" },
    { value: "Malawi", label: "Malawi" },
    { value: "Maldives", label: "Maldives" },
    { value: "Mali", label: "Mali" },
    { value: "Malte", label: "Malte" },
    { value: "Maroc", label: "Maroc" },
    { value: "Marshall", label: "Marshall" },
    { value: "Maurice", label: "Maurice" },
    { value: "Mauritanie", label: "Mauritanie" },
    { value: "Mexique", label: "Mexique" },
    { value: "Micronesie", label: "Micronésie" },
    { value: "Moldavie", label: "Moldavie" },
    { value: "Monaco", label: "Monaco" },
    { value: "Mongolie", label: "Mongolie" },
    { value: "Mozambique", label: "Mozambique" },
    { value: "Namibie", label: "Namibie" },
    { value: "Nauru", label: "Nauru" },
    { value: "Nepal", label: "Népal" },
    { value: "Nicaragua", label: "Nicaragua" },
    { value: "Niger", label: "Niger" },
    { value: "Nigeria", label: "Nigeria" },
    { value: "Niue", label: "Niue" },
    { value: "Norvège", label: "Norvège" },
    { value: "Nouvelle-Zelande", label: "Nouvelle-Zélande" },
    { value: "Oman", label: "Oman" },
    { value: "Ouganda", label: "Ouganda" },
    { value: "Ouzbekistan", label: "Ouzbékistan" },
    { value: "Pakistan", label: "Pakistan" },
    { value: "Panama", label: "Panama" },
    { value: "Papouasie-Nouvelle_Guinee", label: "Papouasie - Nouvelle Guinée" },
    { value: "Paraguay", label: "Paraguay" },
    { value: "Pays-Bas", label: "Pays-Bas" },
    { value: "Perou", label: "Pérou" },
    { value: "Philippines", label: "Philippines" },
    { value: "Pologne", label: "Pologne" },
    { value: "Portugal", label: "Portugal" },
    { value: "Qatar", label: "Qatar" },
    { value: "Republique_centrafricaine", label: "République centrafricaine" },
    { value: "Republique_dominicaine", label: "République dominicaine" },
    { value: "Republique_tcheque", label: "République tchèque" },
    { value: "Roumanie", label: "Roumanie" },
    { value: "Royaume-Uni", label: "Royaume-Uni" },
    { value: "Russie", label: "Russie" },
    { value: "Rwanda", label: "Rwanda" },
    { value: "Saint-Christophe-et-Nieves", label: "Saint-Christophe-et-Niévès" },
    { value: "Sainte-Lucie", label: "Sainte-Lucie" },
    { value: "Saint-Marin", label: "Saint-Marin " },
    { value: "Saint-Siège", label: "Saint-Siège, ou leVatican" },
    { value: "Saint-Vincent-et-les_Grenadines", label: "Saint-Vincent-et-les Grenadines" },
    { value: "Salomon", label: "Salomon" },
    { value: "Salvador", label: "Salvador" },
    { value: "Samoa_occidentales", label: "Samoa occidentales" },
    { value: "Sao_Tome-et-Principe", label: "Sao Tomé-et-Principe" },
    { value: "Senegal", label: "Sénégal" },
    { value: "Serbie", label: "Serbie" },
    { value: "Seychelles", label: "Seychelles" },
    { value: "Sierra_Leone", label: "Sierra Leone" },
    { value: "Singapour", label: "Singapour" },
    { value: "Slovaquie", label: "Slovaquie" },
    { value: "Slovenie", label: "Slovénie" },
    { value: "Somalie", label: "Somalie" },
    { value: "Soudan", label: "Soudan" },
    { value: "Sri_Lanka", label: "Sri Lanka" },
    { value: "Sued", label: "Suède" },
    { value: "Suisse", label: "Suisse" },
    { value: "Suriname", label: "Suriname" },
    { value: "Swaziland", label: "Swaziland" },
    { value: "Syrie", label: "Syrie" },
    { value: "Tadjikistan", label: "Tadjikistan" },
    { value: "Tanzanie", label: "Tanzanie" },
    { value: "Tchad", label: "Tchad" },
    { value: "Thailande", label: "Thaïlande" },
    { value: "Togo", label: "Togo" },
    { value: "Tonga", label: "Tonga" },
    { value: "Trinite-et-Tobago", label: "Trinité-et-Tobago" },
    { value: "Tunisie", label: "Tunisie" },
    { value: "Turkmenistan", label: "Turkménistan" },
    { value: "Turquie", label: "Turquie" },
    { value: "Tuvalu", label: "Tuvalu" },
    { value: "Ukraine", label: "Ukraine" },
    { value: "Uruguay", label: "Uruguay" },
    { value: "Vanuatu", label: "Vanuatu" },
    { value: "Venezuela", label: "Venezuela" },
    { value: "Viet_Nam", label: "Viêt Nam" },
    { value: "Yemen", label: "Yémen" },
    { value: "Yougoslavie", label: "Yougoslavie" },
    { value: "Zaire", label: "Zaïre" },
    { value: "Zambie", label: "Zambie" },
    { value: "Zimbabwe", label: "Zimbabwe" }
]
export const getYears = () => {
    let dateYears = []
    for (let index = moment().format('YYYY') - 13; index >= 1930; index--) {
        dateYears.push({ value: index, label: index })
    }
    return dateYears
}
export const getDays = (month, year) => {
    let dateDays = []


    for (let index = 1; index <= 31; index++) {
        if (index < 10) {
            dateDays.push({ value: '0' + index, label: index })
        } else {
            dateDays.push({ value: index, label: index })
        }
    }

    if (month == '04' || month == '06' || month == '09' || month == '11') {
        dateDays = []
        for (let index = 1; index <= 30; index++) {
            if (index < 10) {
                dateDays.push({ value: '0' + index, label: index })
            } else {
                dateDays.push({ value: index, label: index })
            }
        }

    } else if (month == '02') {
        dateDays = []
        if (moment(year).format('YYYY') % 4 === 0) {
            for (let index = 1; index <= 29; index++) {
                if (index < 10) {
                    dateDays.push({ value: '0' + index, label: index })
                } else {
                    dateDays.push({ value: index, label: index })
                }
            }

        } else {
            for (let index = 1; index <= 28; index++) {
                if (index < 10) {
                    dateDays.push({ value: '0' + index, label: index })
                } else {
                    dateDays.push({ value: index, label: index })
                }
            }

        }
    }

    return dateDays
}
export const getMonth = () => {
    let dateMonth = []
    for (let index = 1; index <= 12; index++) {
        if (index < 10) {
            dateMonth.push({ value: "0" + index, label: index })

        } else {
            dateMonth.push({ value: index, label: index })
        }

    }
    return dateMonth
}
export const typeArticleAR = (typeFR) => {
    switch (typeFR) {
        case 'Football': return 'كرة القدم'
        case 'BASKETBALL': return 'كرة السلة'
        case 'TENNIS': return 'تنس'
        case 'Athlétisme': return 'عدو'
        case 'BaseBall': return 'كرة الضرب'
        case 'BOXE': return 'الملاكمة'
        case 'GOLF': return 'الجولف'
        case 'JUDO': return 'الجودو'
        case 'BMX': return 'سباق الدراجات'
        case 'Bowling': return 'البولنغ'
        case 'CompétitionAutomobile': return 'سباقات السيارات'
        case 'Course': return 'العدو الريفي'
        case 'Cross': return 'ركوب الخيل'
        case 'Footballaméricain': return 'كرة القدم الأمريكية'
        case 'Hapkido': return 'هايكيدو'
        case 'Karting': return 'سباق السيارات الصغيرة'
        case 'Marathon': return 'ماراطون'
        case 'MMA': return 'فنون قتالية مختلطة'
        case 'Netball': return 'كرة الشبكة'
        case 'Natation': return 'سباحة'
        case 'Ping-Pong': return 'تنس الطاولة'
        case 'RinkHockey': return 'حلبة الهوكي'
        case 'Saut-H': return 'القفز العالي'
        case 'Saut-L': return 'الوثب الطويل'
        case 'Rugby': return 'الرغبي'
        case 'Cyclisme': return 'ركوب الدراجات'
        case 'Auto-Moto': return 'سباق الدراجات النارية'
        case 'Sports-hiver': return 'التزلج على الجليد'
        case 'Équitation': return 'ركوب الخيل'
        case 'MotoGP': return 'سباق الجائزة الكبرى للدراجات النارية'
        case 'Formule-1': return 'فورمولا 1'
        case 'Course-Hippique': return 'سباق خيول'
        case 'Handball': return 'كرة اليد'
        case 'Jeux-Olympiques': return 'الألعاب الأولمبية'
        case 'Volleyball': return 'كرة الطائرة'
        case 'Sports-Universitaires': return 'الرياضة الجامعية'
        default:
            return 'TOUS LES SPORTS';
    }
}
export const transformeFRToAR = (MOT) => {

    // for (let i = 1; i <= 100; i++) { //Day
    //     if (MOT === 'Journée ' + i) return 'الجولة ' + i
    // }

    if (MOT.split(' ')[0] === 'Journée') {
        return 'الجولة ' + MOT.split(' ')[1]
    } else if (MOT.split(' ')[0] === 'Groupe') {
        return 'المجموعة ' + MOT.split(' ')[1]
    } else if (MOT === 'Aller') {
        return 'ذهاب'
    } else if (MOT === 'Retour') {
        return 'إياب'
    } else if (MOT === 'Est') {
        return 'شرق'
    } else if (MOT === 'Ouest') {
        return 'غرب'
    } else if (MOT === 'Centre') {
        return 'مركز'
    } else {
        //Tour  , Final , Qualif
        if (MOT === '1er tour') {
            return 'الدور الاول'
        } else if (MOT === '2e tour') {
            return 'الدور الثاني'
        } else if (MOT === '3e tour') {
            return 'الدور الثالث'
        } else if (MOT === '4e tour') {
            return 'الدور الرابع'
        } else if (MOT === '5e tour') {
            return 'الدور الخامس'
        } else if (MOT === '6e tour') {
            return 'الدورالسادس'
        } else if (MOT === '7e tour') {
            return 'الدور السابع'
        } else if (MOT === '8e tour') {
            return 'الدور الثامن'
        } else if (MOT === '16e de finale') {
            return 'الدور السادس عشر'
        } else if (MOT === 'Tour préliminaire') {
            return 'الدور التمهيدي'
        } else if (MOT === 'Tour préliminaire 1') {
            return 'الدور التمهيدي الأول'
        } else if (MOT === 'Tour préliminaire 2') {
            return 'الدور التمهيدي الثاني'
        } else if (MOT === 'Tour de qualification') {
            return 'الدور التصفوي'
        } else if (MOT === 'Tour de qualification 1') {
            return 'الدور التصفوي الأول'
        } else if (MOT === 'Tour de qualification 2') {
            return 'الدور التصفوي الثاني'
        } else if (MOT === 'Tour de qualification 3') {
            return 'الدور التصفوي الثالث'
        } else if (MOT === 'Tour de qualification 4') {
            return 'الدور التصفوي الرابع'
        } else if (MOT === 'Barrages') {
            return 'الملحق'
        } else if (MOT === '1/128 finale') {
            return 'دور 1/128'
        } else if (MOT === '1/64 finale') {
            return 'دور 1/64'
        } else if (MOT === '1/32 finale') {
            return 'الدور الثاني والثلاثين'
        } else if (MOT === '1/16 finale') {
            return 'الدورالسادس عشر'
        } else if (MOT === '1/8 finale') {
            return 'الثمن النهائي'
        } else if (MOT === 'Quart de finale') {
            return 'الربع النهائي'
        } else if (MOT === 'Demi-finale') {
            return 'النصف النهائي'
        } else if (MOT === '3e place') {
            return 'تحديد المركز الثالث'
        } else if (MOT === 'FINALE') {
            return 'النهائي'
        } else {
            return MOT
        }
    }
    // Group
    // if (MOT.split(' ')[0] === 'Groupe') return 'المجموعة ' + alphabetToAR(MOT.split(' ')[1])
    // if (MOT.split(' ')[0] === 'Groupe') return 'المجموعة ' + MOT.split(' ')[1]
}
export const alphabetToAR = (Character) => {
    if (Character === 'A') return 'أ'
    if (Character === 'B') return 'ب'
    if (Character === 'C') return 'ج'
    if (Character === 'D') return 'د'
    if (Character === 'E') return 'ر'
    if (Character === 'F') return 'س'
    if (Character === 'G') return 'ص'
    if (Character === 'H') return 'ط'
}
//facebook
export const FACEBOOK_APP_ID = "357191748915887"
//linkedin
export const LINKEDIN_CLIENT_ID = "77136udoij0a9n"
export const LINKEDIN_CLIENT_SECRET = "VjN4dMJh024uZYLI"
//twitter
export const TWITTER_CONSUMER_KEY = 'tWeKgFXm5MMk2P8hIkNmzUy5E'
export const TWITTER_CONSUMER_SECRET = "WeZmntNXZvYLzgDoxf1q8W4KcL8VFa7z0ZygkELAS0nUqVXUAh"
//google Recaptcha
export const GOOGLE_RECAPTCHA_SITE_KEY = "6LdM7ugZAAAAAHvdYXZo5jN8Hx73xANgozBqjmv5"

export const switchEquipeNationale = (Cat) => {
    switch (Cat) {
        case 'EN A': return 'المنتخب الوطني الأول'
        case 'EN A`': return 'المنتخب الوطني المحلي'
        case 'EN U23': return 'الفريق الوطني لأقل من  U23 '
        case 'EN JEUNES': return 'الفريق الوطني للشباب'
        case 'EN FIMININE': return 'الفريق الوطني النسوي'
        case 'EN MELITAIRE': return 'الفريق الوطني العسكري'
        case 'LES PROFESSIONNELS': return 'المحترفين'
        case 'TALENT': return 'مواهب'
        default:
            return 'Equipe Nationale';
    }
}
