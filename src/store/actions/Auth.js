import * as actionTypes from './actionTypes';
import Axios from 'axios'
import { API_LINK } from '../../utils/constants';


import { addEchecAlert, successAlert, errorMessageAlert } from '../../components/AlertSwal/AlertSwal'

export const signUpSucces = () => {
    return {
        type: actionTypes.SIGNUP_SUCCESS
    };
};

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (token, userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token,
        userId: userId,
        //role: role
    };
};
export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

//asyncron code

export const auth = (idetifiant, type) => {
    let url = 'v1/login'
    if (type === 'facebook') {
        url = 'v1/facebooklogin'
    } else if (type === 'linkedin') {
        url = 'v1/linkedinlogin'
    } else if (type === 'twitter') {
        url = 'v1/twitterlogin'
    }
    return dispatch => {
        dispatch(authStart());

        Axios.post(API_LINK + url, idetifiant)
            .then(res => {

                if (res.data.message === "utilisateur non valide") {
                    dispatch(authFail("compte invalide"))
                } else {

                    localStorage.setItem('token', res.data.token)
                    localStorage.setItem('userId', res.data.user._id)
                    //localStorage.setItem('role', res.data.user.type)
                    dispatch(authSuccess(res.data.token, res.data.user._id, res.data.user.type))
                }
            }
            ).catch(err => {
                console.log('erreur', err.response)
                if (err.response.status === 401) {

                    errorMessageAlert(`une erreur s'est produite lors de votre authentification`, err.response.data.message)
                } else {
                    errorMessageAlert(`une erreur s'est produite lors de votre authentification`, err)
                }

                dispatch(authFail(err))
            })
    }
}
export const signUp = (userData) => {

    return dispatch => {
        dispatch(authStart());
        const newUser = {
            ...userData,
            lien: window.location.origin + '/conftirmation_Email',
        }
      
        Axios.post(API_LINK + 'v1/user', newUser)
            .then(res => {

                if (res.data.message) {
                    dispatch(authFail(res.data.message))
                } else {
                    dispatch(signUpSucces());
                    successAlert('Veuillez consulter votre boite mail un message à été envoyé pour valider votre compte')
                }
            })
            .catch(err => {
                if(err.response.status===400){
                    dispatch(authFail(err))
                    addEchecAlert(err.response.data.message, err)
                }else{
                    dispatch(authFail(err))
                    addEchecAlert("Une erreur s'est produite lors de votre inscription, veuillez réessayer", err)
                }
                
                
            })
    }
}
export const validateEmail = (isValide, userID) => {
    return dispatch => {
        dispatch(authStart());
        const validateUser = {
            isvalid: isValide,
        }
        Axios.put(API_LINK + 'v1/confirm/' + userID, validateUser)
            .then(res => {
                localStorage.setItem('token', res.data.token)
                localStorage.setItem('userId', res.data.user._id)
                localStorage.setItem('role', res.data.user.type)
                successAlert('votre compte  est valider')
                dispatch(authSuccess(res.data.token, res.data.user._id, res.data.user.type))
            })
            .catch(err => {
                addEchecAlert("Une erreur s'est produite lors de la validation de votre compte , veuillez réessayer", err)
                dispatch(authFail(err))
            })
    }
}


export const logout = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('userId')
    // localStorage.removeItem('role')
    return {
        type: actionTypes.AUTH_LOGOUT

    }
}
export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token')
        if (!token) {
            dispatch(logout())
        } else {
            const userId = localStorage.getItem('userId')
            //const role = localStorage.getItem('role')
            dispatch(authSuccess(token, userId))

        }

    }
}

// export const checkAuthTimeout=(expitarionDate)=>{
//     return dispatch=>{
//         setTimeout(() => {
//             dispatch(logout());
//         }, expirationDate*1000);
//     }
// }
export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    }
}