import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import Home from './pages/Home';
import Football from './pages/Football';
import Contact from './pages/Contact';
import Pub from './pages/Pub';
import Notfound from './pages/Notfound';
import Threads from './pages/Threads';
import Login from './pages/Login/index';
import Register from './pages/Register';
import PasswordRecovery from './pages/PasswordRecovery';
import Player from './pages/Player';
import Team from './pages/Team';
import Match from './pages/Match';
import ArticlePage from './pages/Article';
import CalendarPage from './pages/Calendar';
import SessionStatics from './pages/Statics/Session';
import DayStatics from './pages/Statics/Day';
import TeamStatics from './pages/Statics/Team';
import CoupePage from './pages/Coupe';
import ManOfMatch from './pages/manOfMatch';
import GroupstagePage from './pages/Groupestage';
import QualificationStagePage from './pages/Qualificationstage';
import FinalPhase from './pages/FinalPhase';
import Interviews from './pages/Interviews';
import GuestOfMonth from './pages/GuestOfMonth';
import Mercato from './pages/Mercato';
import Chrono from './pages/Chrono';
import Discipline from './pages/Discipline';
import TVprogram from './pages/TVprogram';
import Palmares from './pages/Palmares';
import LiveScore from './pages/LiveScore';
import PlayersNotes from './pages/PlayersNotes';
import GeneraleRanking from './pages/GeneraleRanking';
import PlayerOfMonth from './pages/PlayerOfMonth';
import Journal from './pages/Journal';
import LaQuestionDuJour from './pages/LaQuestionDuJour';
import EventList from './pages/EventList';
import StartNote from './pages/StartNote';
import ShowPlayerNoteResult from './pages/ShowPlayerNoteResult';
import POMLaureats from './pages/PlayerOfMonth/Laureats';
import POMPalmares from './pages/PlayerOfMonth/Palmares';
import POMReglement from './pages/PlayerOfMonth/Reglement';
import AllDebatsPage from './pages/Debat/all';
import Logout from './pages/Login/Logout'
import DebatPage from './pages/Debat';
import ProfilePage from './pages/Profile';
import NewPasswordPage from './pages/NewPassword';
import PlayersRanking from './pages/PlayersRanking';
import PasserRanking from './pages/PasserRanking';
import Equipes from './pages/Equipes';
import TournoiQualif from './pages/TournoiQualificationPage';
import conftirmationEmailPage from './pages/confimationEmailPage/conftirmationEmailPage'
import { connect } from 'react-redux';
import * as actions from './store/actions/index';
import TermsAndConditions from './pages/TermsAndConditions';
import PrivacyPolicy from './pages/PrivacyPolicy';
import SearchPage from './pages/Search';
import FansSondagesPage from './pages/FansSondages';
import ChangePasswordPage from './pages/Profile/ChangePassword';
import LanguagesPage from './pages/Profile/Languages';
import NotificationsPage from './pages/Profile/Notifications';
import NewsletterPage from './pages/Profile/Newsletter';
import { LinkedInPopUp } from 'react-linkedin-login-oauth2';
import MetaDecorator from '../src/components/metaDecorator/metaDecorator'
// import language library
import i18n from './i18n';

// import page preloader
import { FullPreloader } from './components/Loading';

// import css files
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// import layout(s)
import MainLayout from './components/Layouts/MainLayout';

// import funtions
import { getCookie, setCookie } from './utils/functions';

// pour le hours de article 
import moment from 'moment'
import 'moment/locale/fr'
moment.locale('fr')

class App extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      // lang
      langLoaded: false,
      testRender : false
    }
  }

  languageNeeded() {
    let lang = window.location.href.split('?')[1];
    if (lang === 'ar' && getCookie('lang') !== 'ar') {
      setCookie('lang', 'ar', 10);
    }
    else if (lang === 'fr' && getCookie('lang') !== 'fr') {
      setCookie('lang', 'fr', 10);
    }
  }

  componentDidMount() {

    this.props.onTryAutoSignup();
    setInterval(this.renderAutoRender, 10000);


    // load language
    i18n.loadNamespaces('translations', () => {
      this.setState({
        langLoaded: true
      })
    });

    this.detectLanguageStyle();
    this.languageNeeded();
    this.detectMode();
  }

  detectLanguageStyle() {
    if (getCookie('lang') === 'ar') {
      var cssId = 'arCss';  // you could encode the css path itself to generate id..
      var cssId2 = 'arCss2';  // you could encode the css path itself to generate id..
      if (!document.getElementById(cssId)) {
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        var link2 = document.createElement('link');
        link.id = cssId;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = 'https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css';
        link.media = 'all';

        link2.id = cssId2;
        link2.rel = 'stylesheet';
        link2.type = 'text/css';
        link2.href = '/assets/css/rtl.css';
        link2.media = 'all';

        head.appendChild(link);
        head.appendChild(link2);
      }
    }
  }

  detectMode() {
    if (getCookie('darkmode') === 'on') {
      var cssId = 'modeCss';  // you could encode the css path itself to generate id..
      if (!document.getElementById(cssId)) {
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        var link2 = document.createElement('link');
        link.id = cssId;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = '/assets/css/dark.css';
        link.media = 'all';

        head.appendChild(link);
      }
    }
  }

  renderAutoRender = () => {
   if(!this.state.testRender) {
     this.setState({
      testRender  : !this.state.testRender
     })
   }
  }


  render() {
    // short state
    const s = this.state;

    // check language if loaded for remove preloader
    if (!s.langLoaded) {
      return <FullPreloader />;
    }

    let routes =

      <Route exact path="/:path?/:path?/:path?/:path?">
        <MainLayout lang={i18n}>
          <Switch>
            <Route exact path="/" render={(props) => <Home {...props} lang={i18n} />} />
            <Route exact path="/football" render={(props) => <Football {...props} lang={i18n} />} />
            <Route path="/contact" render={(props) => <Contact {...props} lang={i18n} />} />
            <Route path="/espaces_publicitaires" render={(props) => <Pub {...props} lang={i18n} />} />
            <Route path="/terms_et_conditions" render={(props) => <TermsAndConditions {...props} lang={i18n} />} />
            <Route path="/Politique_de_confidentialité" render={(props) => <PrivacyPolicy {...props} lang={i18n} />} />
            <Route path="/actualite/:idtournoi" render={(props) => <Threads {...props} lang={i18n} />} />
            <Route path="/joueur/:id" render={(props) => <Player {...props} lang={i18n} />} />
            <Route path="/equipe/:id" render={(props) => <Team {...props} lang={i18n} />} />
            <Route path="/match/:id" render={(props) => <Match {...props} lang={i18n} />} />
            <Route path="/article/:id" render={(props) => <ArticlePage {...props} lang={i18n} />} />
            <Route path="/calendrier/:tournoi/:group" render={(props) => <CalendarPage {...props} lang={i18n} />} />
            <Route path="/coupe" render={(props) => <CoupePage {...props} lang={i18n} />} />
            <Route path="/homme_de_match" render={(props) => <ManOfMatch {...props} lang={i18n} />} />
            <Route path="/phase_de_groupe/:tournoi" render={(props) => <GroupstagePage {...props} lang={i18n} />} />
            <Route path="/phase_de_qualification/:tournoi" render={(props) => <QualificationStagePage {...props} lang={i18n} />} />
            <Route path="/phase_finale/:tournoi" render={(props) => <FinalPhase {...props} lang={i18n} />} />
            <Route path="/tournoi_qualificatif/:tournoi" render={(props) => <TournoiQualif {...props} lang={i18n} />} />
            <Route path="/interviews" render={(props) => <Interviews {...props} lang={i18n} />} />
            <Route path="/invite_de_mois" render={(props) => <GuestOfMonth {...props} lang={i18n} />} />
            <Route path="/mercato/:tournoi?" render={(props) => <Mercato {...props} lang={i18n} />} />
            <Route path="/discipline" render={(props) => <Discipline {...props} lang={i18n} />} />
            <Route path="/Chrono" render={(props) => <Chrono {...props} lang={i18n} />} />
            <Route path="/programme_TV" render={(props) => <TVprogram {...props} lang={i18n} />} />
            <Route path="/palmares/:tournoi" render={(props) => <Palmares {...props} lang={i18n} />} />
            <Route path="/live_score" render={(props) => <LiveScore {...props} lang={i18n} />} />
            <Route path="/notes_des_joueurs" render={(props) => <PlayersNotes {...props} lang={i18n} />} />
            <Route path="/voir_les_resultats/:id" render={(props) => <ShowPlayerNoteResult {...props} lang={i18n} />} />
            <Route path="/classement_generale/:idtournoi/:groupleague" render={(props) => <GeneraleRanking {...props} lang={i18n} />} />
            <Route path="/joueur_du_mois/vote" render={(props) => <PlayerOfMonth {...props} lang={i18n} />} />

            {/* start statics routes */}
            <Route path="/Statistiques/saison/:tournoi/:group" render={(props) => <SessionStatics {...props} lang={i18n} />} />
            <Route path="/Statistiques/journee/:tournoi/:group" render={(props) => <DayStatics {...props} lang={i18n} />} />
            <Route path="/Statistiques/equipe/:tournoi/:group" render={(props) => <TeamStatics {...props} lang={i18n} />} />


            {/* end statics routes */}
            <Route path="/login" render={(props) => <Login {...props} lang={i18n} />} />
            {/* by tarik */}

            <Route path="/register" render={(props) => <Register {...props} lang={i18n} />} />

            <Route path="/journal" render={(props) => <Journal {...props} lang={i18n} />} />
            <Route path="/question_du_jour" render={(props) => <LaQuestionDuJour {...props} lang={i18n} />} />
            <Route path="/competitions" render={(props) => <EventList {...props} lang={i18n} />} />
            <Route path="/joueur_du_mois/laureats" render={(props) => <POMLaureats {...props} lang={i18n} />} />
            <Route path="/joueur_du_mois/palmares" render={(props) => <POMPalmares {...props} lang={i18n} />} />
            <Route path="/joueur_du_mois/reglement" render={(props) => <POMReglement {...props} lang={i18n} />} />
            <Route path="/debats" render={(props) => <AllDebatsPage {...props} lang={i18n} />} />

            <Route path="/classement_buteurs/:idtournoi/:groupleague" render={(props) => <PlayersRanking {...props} lang={i18n} />} />
            <Route path="/classement_passeurs/:idtournoi/:groupleague" render={(props) => <PasserRanking {...props} lang={i18n} />} />
            <Route path="/equipes/:tournoi/:group" render={(props) => <Equipes {...props} lang={i18n} />} />

            <Route path="/logout" component={Logout} />
            <Route path="/password_recovery" render={(props) => <PasswordRecovery {...props} lang={i18n} />} />
            <Route path="/new_password/:id" render={(props) => <NewPasswordPage {...props} lang={i18n} />} />
            <Route path="/conftirmation_Email/:id" component={conftirmationEmailPage} />
            <Route path="/search" render={(props) => <SearchPage {...props} lang={i18n} />} />
            <Route path="/fans_sondages" render={(props) => <FansSondagesPage {...props} lang={i18n} />} />

            < Route exact path="/linkedin" component={LinkedInPopUp} />
            <Route component={Notfound} />
          </Switch>
        </MainLayout>
      </Route>


   if (this.props.isAuthenticated) {
      routes =

        <Route exact path="/:path?/:path?/:path?/:path?">
          <MainLayout lang={i18n}>
            <Switch>
              <Route exact path="/" render={(props) => <Home {...props} lang={i18n} />} />
              <Route exact path="/football" render={(props) => <Football {...props} lang={i18n} />} />
              <Route path="/contact" render={(props) => <Contact {...props} lang={i18n} />} />
              <Route path="/espaces_publicitaires" render={(props) => <Pub {...props} lang={i18n} />} />
              <Route path="/terms_et_conditions" render={(props) => <TermsAndConditions {...props} lang={i18n} />} />
              <Route path="/Politique_de_confidentialité" render={(props) => <PrivacyPolicy {...props} lang={i18n} />} />
              <Route path="/actualite/:idtournoi" render={(props) => <Threads {...props} lang={i18n} />} />
              <Route path="/joueur/:id" render={(props) => <Player {...props} lang={i18n} />} />

              <Route path="/equipe/:id" render={(props) => <Team {...props} lang={i18n} />} />
              <Route path="/match/:id" render={(props) => <Match {...props} lang={i18n} />} />
              <Route path="/article/:id" render={(props) => <ArticlePage {...props} lang={i18n} />} />
              <Route path="/calendrier/:tournoi/:group" render={(props) => <CalendarPage {...props} lang={i18n} />} />
              <Route path="/coupe" render={(props) => <CoupePage {...props} lang={i18n} />} />
              <Route path="/homme_de_match" render={(props) => <ManOfMatch {...props} lang={i18n} />} />
              <Route path="/phase_de_groupe/:tournoi" render={(props) => <GroupstagePage {...props} lang={i18n} />} />
              <Route path="/phase_de_qualification/:tournoi" render={(props) => <QualificationStagePage {...props} lang={i18n} />} />
              <Route path="/phase_finale/:tournoi" render={(props) => <FinalPhase {...props} lang={i18n} />} />
              <Route path="/tournoi_qualificatif/:tournoi" render={(props) => <TournoiQualif {...props} lang={i18n} />} />
              <Route path="/interviews" render={(props) => <Interviews {...props} lang={i18n} />} />
              <Route path="/invite_de_mois" render={(props) => <GuestOfMonth {...props} lang={i18n} />} />
              <Route path="/mercato/:tournoi?" render={(props) => <Mercato {...props} lang={i18n} />} />
              <Route path="/discipline" render={(props) => <Discipline {...props} lang={i18n} />} />
              <Route path="/Chrono" render={(props) => <Chrono {...props} lang={i18n} />} />
              <Route path="/programme_TV" render={(props) => <TVprogram {...props} lang={i18n} />} />
              <Route path="/palmares/:tournoi" render={(props) => <Palmares {...props} lang={i18n} />} />
              <Route path="/live_score" render={(props) => <LiveScore {...props} lang={i18n} />} />
              <Route path="/notes_des_joueurs" render={(props) => <PlayersNotes {...props} lang={i18n} />} />
              <Route path="/voir_les_resultats/:id" render={(props) => <ShowPlayerNoteResult {...props} lang={i18n} />} />
              <Route path="/classement_generale/:idtournoi/:groupleague" render={(props) => <GeneraleRanking {...props} lang={i18n} />} />
              <Route path="/joueur_du_mois/vote" render={(props) => <PlayerOfMonth {...props} lang={i18n} />} />
              {/* start statics routes */}
              <Route path="/Statistiques/saison/:tournoi/:group" render={(props) => <SessionStatics {...props} lang={i18n} />} />
              <Route path="/Statistiques/journee/:tournoi/:group" render={(props) => <DayStatics {...props} lang={i18n} />} />
              <Route path="/Statistiques/equipe/:tournoi/:group" render={(props) => <TeamStatics {...props} lang={i18n} />} />
              {/* end statics routes */}
              <Route path="/login" render={(props) => <Login {...props} lang={i18n} />} />
              <Route path="/password_recovery" render={(props) => <PasswordRecovery {...props} lang={i18n} />} />
              <Route path="/register" render={(props) => <Register {...props} lang={i18n} />} />

              <Route path="/journal" render={(props) => <Journal {...props} lang={i18n} />} />
              <Route path="/question_du_jour" render={(props) => <LaQuestionDuJour {...props} lang={i18n} />} />
              <Route path="/competitions" render={(props) => <EventList {...props} lang={i18n} />} />
              <Route path="/joueur_du_mois/laureats" render={(props) => <POMLaureats {...props} lang={i18n} />} />
              <Route path="/joueur_du_mois/palmares" render={(props) => <POMPalmares {...props} lang={i18n} />} />
              <Route path="/joueur_du_mois/reglement" render={(props) => <POMReglement {...props} lang={i18n} />} />
              <Route path="/debats" render={(props) => <AllDebatsPage {...props} lang={i18n} />} />
              <Route path="/debat/:debat" render={(props) => <DebatPage {...props} lang={i18n} />} />

              <Route path="/classement_buteurs/:idtournoi/:groupleague" render={(props) => <PlayersRanking {...props} lang={i18n} />} />
              <Route path="/classement_passeurs/:idtournoi/:groupleague" render={(props) => <PasserRanking {...props} lang={i18n} />} />
              <Route path="/equipes/:tournoi/:group" render={(props) => <Equipes {...props} lang={i18n} />} />
              {/* routes aprés autentification  */}
              <Route path="/commencer_a_noter/:id" render={(props) => <StartNote {...props} lang={i18n} />} />
              <Route path="/logout" component={Logout} />
              <Route path="/profile" render={(props) => <ProfilePage {...props} lang={i18n} />} />
              <Route path="/conftirmation_Email/:id" component={conftirmationEmailPage} />
              <Route path="/search" render={(props) => <SearchPage {...props} lang={i18n} />} />
              <Route path="/fans_sondages" render={(props) => <FansSondagesPage {...props} lang={i18n} />} />
              <Route path="/change_password" render={(props) => <ChangePasswordPage {...props} lang={i18n} />} />
              <Route path="/languages" render={(props) => <LanguagesPage {...props} lang={i18n} />} />
              <Route path="/notifications" render={(props) => <NotificationsPage {...props} lang={i18n} />} />
              <Route path="/newsletter" render={(props) => <NewsletterPage {...props} lang={i18n} />} />
              <Route component={Notfound} />
            </Switch>
          </MainLayout>
        </Route>


    }
console.log("test Madjiiid", this.state.testRender ? <MetaDecorator/> : null)
    return (
      <div> 
      <div> 
        
         {routes}
      </div>
      {/* <div> 
     {this.state.testRender ? <MetaDecorator/> : null} 
   </div> */}
   </div>
    );
  }

}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
}
const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));