import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../../utils/constants'

const initialState = {
    token: null,
    userId: null,
    // role:null,
    error: null,
    loading: false,
    authRedirectPath: '/'


}
const signUpSucces = (state, action) => {
    return updateObject(state, { error: null, loading: false })
}
const authStart = (state, action) => {
    return updateObject(state, { error: null, loading: true });
}

const authSucces = (state, action) => {
    return updateObject(state, {
        token: action.idToken,
        userId: action.userId,
        //role:action.role,
        error: null,
        loading: false,

    });
}

const authFail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    })
}

const setAuthRedirectPath = (state, action) => {
    return updateObject(state, { authRedirectPath: action.path })
}

const authLogout = (state, action) => {
    return updateObject(state, { token: null, userId: null })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SIGNUP_SUCCESS: return signUpSucces(state, action);
        case actionTypes.AUTH_START: return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: return authSucces(state, action);
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_LOGOUT: return authLogout(state, action);
        case actionTypes.SET_AUTH_REDIRECT_PATH: return setAuthRedirectPath(state, action);
        default: return state

    }

}
export default reducer;