import React from 'react';
import { hydrate, render } from "react-dom";
import ReactDOM from 'react-dom';
import App from './App'
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import authReducer from './store/reducers/auth'
import thunk from 'redux-thunk'
import ReactDOMServer from 'react-dom/server';
import { Helmet } from "react-helmet";
import MetaDecorator from '../src/components/metaDecorator/metaDecorator'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
//on peut combiné plusieurs reducer avec combineReducers
const rootReducer = combineReducers({
  auth: authReducer
})
const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
)
)
const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
)
// const rootElement = document.getElementById("root");
// if (rootElement.hasChildNodes()) {
//   hydrate(app, rootElement);
// } else {
//   render(app, rootElement);
// }
// ReactDOMServer.renderToString(<MetaDecorator />);
// const helmet = Helmet.renderStatic();
ReactDOM.render(app, document.getElementById('root'));
